package com.roukanken.veilthroch.util

import strikt.api.Assertion

@JvmName("hasSize_Sequence")
fun <T> Assertion.Builder<Sequence<T>>.hasSize(size: Int) =
    assert("has size $size") {
        when (val count = it.count()) {
            size -> pass()
            else -> fail(description = "but it has %s", actual = count)
        }
    }

@JvmName("get_Sequence")
fun <T> Assertion.Builder<Sequence<T>>.first() =
    get("first element %s") { first() }


