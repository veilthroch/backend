package com.roukanken.veilthroch

import graphql.kickstart.spring.web.boot.GraphQLWebsocketAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration

@SpringBootTest
@ContextConfiguration(classes = [VeilthrochApplication::class, GraphQLWebsocketAutoConfiguration::class])
class VeilthrochApplicationTests
