package com.roukanken.veilthroch.model.tera.common

import io.mockk.mockk
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isNotEqualTo

internal class TeraObjectTest {
    class Type1(override val id: TeraId) : TeraObject()
    class Type2(override val id: TeraId) : TeraObject()

    @Test
    fun `equals is true on same class same id`() {
        val id = mockk<TeraId>()

        val one: TeraObject = Type1(id)
        val two: TeraObject = Type1(id)

        expectThat(one) {
            isEqualTo(two)
            get { hashCode() } isEqualTo two.hashCode()
        }
    }

    @Test
    fun `equals is false on same id but different class`() {
        val id = mockk<TeraId>()

        val one: TeraObject = Type1(id)
        val two: TeraObject = Type2(id)

        expectThat(one) isNotEqualTo two
    }
}