package com.roukanken.veilthroch.model.tera.common

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class TeraIdTest {
    @Test
    fun `as patch keeps data`() {
        val id = TeraId(47, Region.EU, "42")

        val result = id.asPatch()

        expectThat(result) {
            get { region } isEqualTo Region.EU
            get { patch } isEqualTo "42"
        }
    }
}