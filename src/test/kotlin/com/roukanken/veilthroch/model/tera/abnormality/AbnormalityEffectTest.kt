package com.roukanken.veilthroch.model.tera.abnormality

import com.roukanken.veilthroch.mock.AbnormalityMockFactory
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class AbnormalityEffectTest {

    @Test
    fun getValue() {
        val abnormality = AbnormalityMockFactory.defensiveStance()

        expectThat(abnormality.effects) {
            get { get(0).value } isEqualTo 1.25
            get { get(1).value } isEqualTo 30
        }
    }
}