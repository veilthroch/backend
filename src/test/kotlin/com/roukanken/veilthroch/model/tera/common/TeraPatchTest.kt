package com.roukanken.veilthroch.model.tera.common

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

internal class TeraPatchTest {
    @Test
    fun `to object keeps patch and region`() {
        val patch = TeraPatch(Region.EU, "42")

        val result = patch.asObject(47)

        expectThat(result) {
            get { id } isEqualTo 47
            get { region } isEqualTo Region.EU
            get { this.patch } isEqualTo "42"
        }
    }
}