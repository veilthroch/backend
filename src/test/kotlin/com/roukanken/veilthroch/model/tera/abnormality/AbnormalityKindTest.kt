package com.roukanken.veilthroch.model.tera.abnormality

import com.roukanken.veilthroch.mock.AbnormalityMockFactory
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isNotNull
import strikt.assertions.matchesIgnoringCase

internal class AbnormalityKindTest {

    @Test
    @Disabled
    fun `description is rendered`() {
        val kind = AbnormalityMockFactory.defensiveStance().kind

        expectThat(kind.description)
            .isNotNull()
            .matchesIgnoringCase("<font[^>]*>Benefit</font>".toRegex())
    }
}