package com.roukanken.veilthroch.mock

import com.roukanken.veilthroch.model.tera.abnormality.Abnormality
import com.roukanken.veilthroch.model.tera.abnormality.AbnormalityEffect
import com.roukanken.veilthroch.model.tera.abnormality.AbnormalityKind
import com.roukanken.veilthroch.model.tera.common.Region
import com.roukanken.veilthroch.model.tera.common.TeraId
import java.math.BigInteger

object AbnormalityMockFactory {

    fun defensiveStance() = Abnormality(
        id = TeraId(100201, Region.EU, "84"),

        icon = "Icon_Skills.ironwillB_Tex",
        name = "Defensive Stance II",
        tooltip = "Increases Endurance by \$H_W_GOOD\$value\$COLOR_END " +
            "and Balance Factor by\$H_W_GOOD\$value2\$COLOR_END. " +
            "Increases the chance to crit by\$H_W_GOOD15%\$COLOR_END, " +
            "Physical Resistance by\$H_W_GOOD20%\$COLOR_END when attacking monsters. " +
            "Attacks draw\$H_W_GOOD\$value3\$COLOR_END more aggro. " +
            "Immune to push-back from monsters, save for certain special attacks.",

        kind = AbnormalityKind(9, "\$H_S_GOODBenefit\$COLOR_END"),
        priority = 1,
        level = 84,
        time = BigInteger.valueOf(1000),

        isInfinite = true,
        isBuff = true,
        isHidden = false,

        effects = listOf(
            AbnormalityEffect(4, 3, "1.25", 0),
            AbnormalityEffect(9, 2, "30", 0),
            AbnormalityEffect(20, 3, "2.53", 0),
            AbnormalityEffect(209, 3, "0", 0),
            AbnormalityEffect(264, 0, "100297", 0),
            AbnormalityEffect(264, 0, "100298", 0),
            AbnormalityEffect(264, 0, "100299", 0),
            AbnormalityEffect(113, 3, "1.2", 0),
            AbnormalityEffect(264, 0, "100296", 0),
            AbnormalityEffect(1003, 3, "1.2", 0),
        )
    )
}