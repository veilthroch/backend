package com.roukanken.veilthroch.mock

import com.roukanken.veilthroch.datacenter.structure.Datacenter
import com.roukanken.veilthroch.datacenter.structure.DatacenterInputStream

object DatacenterMockFactory {

    private fun readDc(classpath: String) =
        this::class.java.getResourceAsStream(classpath).use { stream ->
            Datacenter(DatacenterInputStream(stream))
        }

    fun minimalDatacenter() = readDc("/datacenter/small.dat")
    fun sampleDatacenter() = readDc("/datacenter/sample.dat")
    fun b84Datacenter() = readDc("/datacenter/B84_EU.dat")
}