package com.roukanken.veilthroch.datacenter.mapping

import com.roukanken.veilthroch.datacenter.structure.Datacenter
import com.roukanken.veilthroch.datacenter.structure.DatacenterAddress
import com.roukanken.veilthroch.datacenter.structure.DatacenterElement
import com.roukanken.veilthroch.mock.DatacenterMockFactory
import com.roukanken.veilthroch.util.first
import com.roukanken.veilthroch.util.hasSize
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.*

internal class DatacenterElementCursorTest {

    @Test
    fun testRootElementCursor() {
        val cursor = DatacenterElementCursor(DatacenterMockFactory.minimalDatacenter())

        expectThat(cursor) {
            get { children() }
                .hasSize(1)
                .first()
                .and {
                    get { name }.isEqualTo("Hello")
                }
        }
    }

    @Test
    fun `element cursor maps to POJO`() {
        val datacenter = mockk<Datacenter>()

        val rootElement = mockk<DatacenterElement>()
        every { datacenter.elements[DatacenterAddress(0, 0)] } returns rootElement

        every { rootElement.attributeCount } returns 4
        every { rootElement.attributeAddress } returns DatacenterAddress(0, 0)

        val cursor = spyk(datacenter.getCursor())

        every { cursor.attributes() } returns sequenceOf(
            mockAttributeCursor("bool", true),
            mockAttributeCursor("int", 42),
            mockAttributeCursor("string", "String"),
            mockAttributeCursor("float", 42.47F)
        )

        every { cursor.children() } returns emptySequence()

        val result = cursor.mapTo(PojoType::class)

        expectThat(result) {
            get { int }.isEqualTo(42)
            get { string }.isEqualTo("String")
            get { bool }.isEqualTo(true)
            get { float }.isEqualTo(42.47F)
        }
    }

    @Test
    fun `element cursor maps to POJO with subelements`() {
        val datacenter = mockk<Datacenter>()

        val rootElement = mockk<DatacenterElement>()
        every { datacenter.elements[DatacenterAddress(0, 0)] } returns rootElement

        val cursor = spyk(datacenter.getCursor())

        every { cursor.attributes() } returns emptySequence()
        every { cursor.children() } returns sequenceOf(
            mockk {
                every { name } returns "Single"
                every { mapTo(PojoType::class) } returns mockk {
                    every { string } returns "Single"
                }
            },
            mockk {
                every { name } returns "Element"
                every { mapTo(PojoType::class) } returns mockk {
                    every { string } returns "Element 1"
                }
            },
            mockk {
                every { name } returns "Element"
                every { mapTo(PojoType::class) } returns mockk {
                    every { string } returns "Element 2"
                }
            }
        )

        val result = cursor.mapTo(PojoSupertype::class)

        expectThat(result) {
            get { single.string } isEqualTo "Single"
            get { elements }.map { it.string }.containsExactlyInAnyOrder("Element 1", "Element 2")
        }
    }

    class MultipleMatching {
        lateinit var single: PojoType
        var singles = emptyList<PojoType>()

        @DcName("Single")
        lateinit var differentName: PojoType
    }

    @Test
    fun `multiple matching properties throw exception`() {
        val datacenter = mockk<Datacenter>()

        val rootElement = mockk<DatacenterElement>()
        every { datacenter.elements[DatacenterAddress(0, 0)] } returns rootElement

        val cursor = spyk(datacenter.getCursor())

        every { cursor.attributes() } returns emptySequence()
        every { cursor.children() } returns sequenceOf(
            mockk {
                every { name } returns "Single"
                every { mapTo(PojoType::class) } returns mockk {
                    every { string } returns "Single"
                }
            }
        )

        expectThrows<MappingException> { cursor.mapTo(MultipleMatching::class) }
            .get { message }
            .isNotNull()
            .containsIgnoringCase("[^a-z]single[^a-z]".toRegex())
            .containsIgnoringCase("[^a-z]singles[^a-z]".toRegex())
            .containsIgnoringCase("[^a-z]differentName[^a-z]".toRegex())
    }

    class ImmutableProperty {
        val single = emptyList<PojoType>()
    }

    @Test
    fun `immutable properties throw exception`() {
        val datacenter = mockk<Datacenter>()

        val rootElement = mockk<DatacenterElement>()
        every { datacenter.elements[DatacenterAddress(0, 0)] } returns rootElement

        val cursor = spyk(datacenter.getCursor())

        every { cursor.attributes() } returns emptySequence()
        every { cursor.children() } returns sequenceOf(
            mockk {
                every { name } returns "Single"
                every { mapTo(PojoType::class) } returns mockk {
                    every { string } returns "Single"
                }
            }
        )

        expectThrows<MappingException> { cursor.mapTo(ImmutableProperty::class) }
    }

    private fun mockAttributeCursor(name: String, value: Any): DatacenterAttributeCursor {
        val attributeCursor = spyk(DatacenterAttributeCursor(mockk(), mockk()))

        every { attributeCursor.name } returns name
        every { attributeCursor.value } returns value

        return attributeCursor
    }
}
