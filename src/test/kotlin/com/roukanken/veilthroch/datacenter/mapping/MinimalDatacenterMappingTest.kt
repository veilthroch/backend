package com.roukanken.veilthroch.datacenter.mapping

import com.roukanken.veilthroch.mock.DatacenterMockFactory
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.contains
import strikt.assertions.isEqualTo
import strikt.assertions.map

class MinimalDatacenter {
    lateinit var hello: MinimalHello
}

class MinimalHello {
    lateinit var from: String
    var sentences: List<MinimalSentence> = emptyList()
}

class MinimalSentence {
    var __value__: String? = null
}

class MinimalDatacenterMappingTest {
    @Test
    fun `minimal datacenter maps to POKOs`() {
        val datacenter = DatacenterMockFactory.minimalDatacenter()

        val result = datacenter.mapTo<MinimalDatacenter>()

        expectThat(result) {
            get { hello }.and {
                get { from } isEqualTo "Rou"
                get { sentences }.map { it.__value__ }.contains("Hello how are you?")
            }
        }
    }
}