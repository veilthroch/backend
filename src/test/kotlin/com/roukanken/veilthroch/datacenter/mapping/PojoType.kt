package com.roukanken.veilthroch.datacenter.mapping

import kotlin.properties.Delegates

class PojoType {
    lateinit var string: String
    var int by Delegates.notNull<Int>()
    var bool by Delegates.notNull<Boolean>()
    var float by Delegates.notNull<Float>()
}

class PojoSupertype {
    lateinit var single: PojoType
    var elements: List<PojoType> = emptyList()
}