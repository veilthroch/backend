package com.roukanken.veilthroch.datacenter.mapping

import com.roukanken.veilthroch.datacenter.structure.Datacenter
import com.roukanken.veilthroch.datacenter.structure.DatacenterAddress
import com.roukanken.veilthroch.datacenter.structure.DatacenterAttribute
import com.roukanken.veilthroch.mock.DatacenterMockFactory
import com.roukanken.veilthroch.util.first
import com.roukanken.veilthroch.util.hasSize
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test
import strikt.api.expect
import strikt.api.expectCatching
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.isA
import strikt.assertions.isEqualTo
import strikt.assertions.isSuccess

class DatacenterAttributeCursorTest {

    @Test
    fun testAttributeCursor() {
        val cursor = DatacenterElementCursor(DatacenterMockFactory.minimalDatacenter())

        expectThat(cursor.children().first()) {
            get { attributes() }
                .hasSize(1)
                .first()
                .and {
                    get { name }.isEqualTo("from")
                    get { value }.isA<String>().isEqualTo("Rou")
                }
        }
    }

    @Test
    fun `attributes map to POJO class`() {

        val attributes = listOf(
            makeAttribute("string", "String"),
            makeAttribute("int", 42),
            makeAttribute("bool", true),
            makeAttribute("float", 42.47F)
        )

        val pojo = PojoType()
        attributes.forEach { it.fillAttribute(pojo) }

        expect {
            catching { pojo.int }.describedAs("getting int").isSuccess().and { isEqualTo(42) }
            catching { pojo.string }.describedAs("getting string").isSuccess().and { isEqualTo("String") }
            catching { pojo.bool }.describedAs("getting bool").isSuccess().and { isEqualTo(true) }
            catching { pojo.float }.describedAs("getting float").isSuccess().and { isEqualTo(42.47F) }
        }
    }

    @Test
    fun `attributes mapping throws on wrong type`() {
        val attribute = makeAttribute("string", 24)

        val pojo = PojoType()
        expectThrows<MappingException> { attribute.fillAttribute(pojo) }
    }

    @Test
    fun `attributes extension throws error on not setting non-nullable attribute`() {
        val attributes = listOf(
            makeAttribute("string", "String"),
            makeAttribute("int", 42),
            makeAttribute("bool", true)
        ).asSequence()

        val pojo = PojoType()
        expectThrows<MappingException> { attributes.fillAttributes(pojo) }
    }

    @Test
    fun `attributes extension ignores non-datacenter attributes`() {
        val attributes = listOf(
            makeAttribute("string", "String"),
            makeAttribute("int", 42),
            makeAttribute("bool", true)
        ).asSequence()

        val pojoSupertype = PojoSupertype()
        expectCatching { attributes.fillAttributes(pojoSupertype) }.isSuccess()
    }

    private fun makeAttribute(name: String, value: Any): DatacenterAttributeCursor {
        val NAME = 42
        val ADDRESS = DatacenterAddress(42, 47)

        when (value) {
            is Int -> Unit
            is Boolean -> Unit
            is String -> Unit
            is Float -> Unit
            else -> throw IllegalArgumentException("Value can be only Int, Boolean, String or Float!")
        }

        val datacenter = mockk<Datacenter>()
        val attribute = mockk<DatacenterAttribute>()

        every { attribute.nameIndex } returns NAME
        every { datacenter.names[NAME] } returns name

        if (value is String) {
            every { attribute.value } returns ADDRESS
            every { datacenter.values[ADDRESS] } returns value
        } else {
            every { attribute.value } returns value
        }

        return DatacenterAttributeCursor(datacenter, attribute)
    }
}
