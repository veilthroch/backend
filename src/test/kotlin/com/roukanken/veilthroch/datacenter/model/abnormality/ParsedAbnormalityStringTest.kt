package com.roukanken.veilthroch.datacenter.model.abnormality

import com.roukanken.veilthroch.datacenter.mapping.mapTo
import com.roukanken.veilthroch.datacenter.model.ParsedDatacenter
import com.roukanken.veilthroch.mock.DatacenterMockFactory
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.first
import strikt.assertions.isEqualTo
import strikt.assertions.isNotEmpty

internal class ParsedAbnormalityStringTest {
    @Test
    fun `parses sample`() {
        val dc = DatacenterMockFactory.sampleDatacenter()

        val parsedDc = dc.mapTo<ParsedDatacenter>()

        expectThat(parsedDc)
            .get { abnormalityStrings }.isNotEmpty()
            .first().get { strings }.isNotEmpty()
            .first().and {
                get { id } isEqualTo 100201
                get { name } isEqualTo "Defensive Stance II"
            }
    }
}