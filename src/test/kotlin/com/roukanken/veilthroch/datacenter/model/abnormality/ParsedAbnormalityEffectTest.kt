package com.roukanken.veilthroch.datacenter.model.abnormality

import com.roukanken.veilthroch.datacenter.mapping.mapTo
import com.roukanken.veilthroch.datacenter.model.ParsedDatacenter
import com.roukanken.veilthroch.mock.DatacenterMockFactory
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.first
import strikt.assertions.isEqualTo
import strikt.assertions.isNotEmpty

internal class ParsedAbnormalityEffectTest {
    @Test
    fun `parses abnormal sample`() {
        val dc = DatacenterMockFactory.sampleDatacenter()

        val parsedDc = dc.mapTo<ParsedDatacenter>()

        expectThat(parsedDc)
            .get { abnormalities }.isNotEmpty()
            .first().get { abnormals }.isNotEmpty()
            .first().get { abnormalityEffects }.isNotEmpty()
            .first().and {
                get { method } isEqualTo 3
                get { tickInterval } isEqualTo 0
                get { type } isEqualTo 4
                get { value } isEqualTo "1.25"
            }
    }
}