package com.roukanken.veilthroch.datacenter.model.abnormality

import com.roukanken.veilthroch.datacenter.mapping.mapTo
import com.roukanken.veilthroch.datacenter.model.ParsedDatacenter
import com.roukanken.veilthroch.mock.DatacenterMockFactory
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.first
import strikt.assertions.isEqualTo
import strikt.assertions.isNotEmpty

internal class ParsedAbnormalityIconTest {
    @Test
    fun `parses sample`() {
        val dc = DatacenterMockFactory.sampleDatacenter()

        val parsedDc = dc.mapTo<ParsedDatacenter>()

        expectThat(parsedDc)
            .get { abnormalityIconData }.isNotEmpty()
            .first().get { icons }.isNotEmpty()
            .first().and {
                get { abnormalityId } isEqualTo 100201
            }
    }
}