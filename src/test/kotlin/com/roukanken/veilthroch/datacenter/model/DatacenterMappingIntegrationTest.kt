package com.roukanken.veilthroch.datacenter.model

import com.roukanken.veilthroch.datacenter.mapping.mapTo
import com.roukanken.veilthroch.logger
import com.roukanken.veilthroch.mock.DatacenterMockFactory
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import strikt.api.expectCatching
import strikt.assertions.filter
import strikt.assertions.flatMap
import strikt.assertions.isNotEmpty
import strikt.assertions.isSuccess

@Tag("integrationTest")
class DatacenterMappingIntegrationTest {
    private val log = logger()

    @Test
    fun `B84 maps without error`() {
        log.error("Reading datacenter")
        val datacenter = DatacenterMockFactory.b84Datacenter()

        log.error("Mapping datacenter")
        expectCatching { datacenter.mapTo<ParsedDatacenter>() }.isSuccess()
            // Defensive stance
            .and { get { abnormalityStrings }.flatMap { it.strings }.filter { it.id == 100201 }.isNotEmpty() }
            // Warrior stance kind
            .and { get { abnormalityKind.strings }.filter { it.id == 9 }.isNotEmpty() }
            // Defensive stance
            .and { get { abnormalities }.flatMap { it.abnormals }.filter { it.id == 100201 }.isNotEmpty() }
            // Defensive stance
            .and { get { abnormalityIconData }.flatMap { it.icons }.filter { it.abnormalityId == 100201 }.isNotEmpty() }
    }
}