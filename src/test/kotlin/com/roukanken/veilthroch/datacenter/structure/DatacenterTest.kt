package com.roukanken.veilthroch.datacenter.structure

import org.junit.jupiter.api.Test
import strikt.api.expectThrows
import java.io.IOException

private const val DATACENTER_URL = "/datacenter/small.dat"

internal class DatacenterTest {

    @Test
    fun `datacenter reads from stream`() {
        val stream = DatacenterInputStream(this::class.java.getResourceAsStream(DATACENTER_URL))
        val datacenter = Datacenter(stream)

        expectThrows<IOException> { stream.readByte() }
    }


}

