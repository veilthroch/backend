package com.roukanken.veilthroch.datacenter.collect

import com.roukanken.veilthroch.datacenter.mapping.mapTo
import com.roukanken.veilthroch.datacenter.model.ParsedDatacenter
import com.roukanken.veilthroch.mock.DatacenterMockFactory
import com.roukanken.veilthroch.model.tera.common.Region
import com.roukanken.veilthroch.model.tera.common.TeraPatch
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.first
import strikt.assertions.isEqualTo
import strikt.assertions.isNotEmpty
import strikt.assertions.isNotNull

internal class AbnormalityCollectorTest {
    @Test
    fun `collect constructs object`() {
        val datacenter = mockk<ParsedDatacenter>()
        val patch = TeraPatch(Region.EU, "42")

        every { datacenter.abnormalityKind } returns mockk {
            every { strings } returns listOf(mockk {
                every { id } returns 9
                every { name } returns "Benefit"
            })
        }

        every { datacenter.abnormalityStrings } returns listOf(mockk {
            every { strings } returns listOf(mockk {
                every { id } returns 100201
                every { name } returns "Defensive Stance II"
                every { tooltip } returns "is awesome"
            })
        })

        every { datacenter.abnormalityIconData } returns listOf(mockk {
            every { icons } returns listOf(mockk {
                every { abnormalityId } returns 100201
                every { iconName } returns "Icon_Skills.ironwillB_Tex"
            })
        })

        every { datacenter.abnormalities } returns listOf(mockk(relaxed = true) {
            every { abnormals } returns listOf(mockk {
                every { id } returns 100201
                every { kind } returns 9
                every { priority } returns 10
                every { level } returns 42
                every { time } returns "420"
                every { infinity } returns true
                every { isBuff } returns false
                every { isShow } returns "True"
                every { abnormalityEffects } returns listOf(mockk {
                    every { type } returns 3
                    every { method } returns 4
                    every { tickInterval } returns 50
                    every { value } returns "48.5"
                })
            })
        })


        val result = AbnormalityCollector.collect(patch, datacenter)


        expectThat(result) {
            isNotEmpty().first().and {
                get { id } isEqualTo patch.asObject(100201)
                get { name } isEqualTo "Defensive Stance II"
                get { icon } isEqualTo "Icon_Skills.ironwillB_Tex"
                get { kind.description } isEqualTo "Benefit"
            }
        }
    }
}

@Tag("integrationTest")
internal class AbnormalityCollectorIntegrationTest {
    @Test
    fun `loads B84`() {
        val datacenter = DatacenterMockFactory.b84Datacenter().mapTo<ParsedDatacenter>()
        val patch = TeraPatch(Region.EU, "42")

        val result = AbnormalityCollector.collect(patch, datacenter)

        expectThat(result.firstOrNull { it.id == patch.asObject(100201) })
            .isNotNull().and {
                get { id } isEqualTo patch.asObject(100201)
                get { isBuff } isEqualTo true
                get { name } isEqualTo "Defensive Stance II"
                get { icon } isEqualTo "Icon_Skills.ironwillB_Tex"
                get { kind.description }.isNotNull()
                get { effects }.isNotEmpty()
            }
    }
}