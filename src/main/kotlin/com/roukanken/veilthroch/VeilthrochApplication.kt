package com.roukanken.veilthroch

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@ConfigurationPropertiesScan
@SpringBootApplication
class VeilthrochApplication

fun main(args: Array<String>) {
    runApplication<VeilthrochApplication>(*args)
}