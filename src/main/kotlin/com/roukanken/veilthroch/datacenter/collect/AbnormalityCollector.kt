package com.roukanken.veilthroch.datacenter.collect

import com.roukanken.veilthroch.datacenter.model.ParsedDatacenter
import com.roukanken.veilthroch.datacenter.model.abnormality.ParsedAbnormalityEffect
import com.roukanken.veilthroch.datacenter.model.common.ParsedString
import com.roukanken.veilthroch.model.tera.abnormality.Abnormality
import com.roukanken.veilthroch.model.tera.abnormality.AbnormalityEffect
import com.roukanken.veilthroch.model.tera.abnormality.AbnormalityKind
import com.roukanken.veilthroch.model.tera.common.TeraPatch

object AbnormalityCollector {

    fun collect(patch: TeraPatch, datacenter: ParsedDatacenter): List<Abnormality> {
        val kinds = datacenter.abnormalityKind.strings
            .map { it.id to mapKind(it) }.toMap()

        val strings = datacenter.abnormalityStrings.flatMap { it.strings }
            .map { it.id to it }.toMap()

        val icons = datacenter.abnormalityIconData.flatMap { it.icons }
            .map { it.abnormalityId to it.iconName }.toMap()

        return datacenter.abnormalities.flatMap { it.abnormals }.map {
            Abnormality(
                id = patch.asObject(it.id),

                icon = icons[it.id],
                name = strings[it.id]?.name,
                tooltip = strings[it.id]?.tooltip,

                kind = kinds.getOrDefault(it.kind, AbnormalityKind(it.id)),
                priority = it.priority,
                level = it.level,
                time = it.time.toBigInteger(),

                isInfinite = it.infinity,
                isBuff = it.isBuff,
                isHidden = it.isShow.toLowerCase() != "true",

                effects = it.abnormalityEffects.map { effect -> mapEffect(effect) }
            )
        }
    }

    private fun mapKind(it: ParsedString) =
        AbnormalityKind(
            id = it.id,
            description = it.name
        )

    private fun mapEffect(effect: ParsedAbnormalityEffect) =
        AbnormalityEffect(
            type = effect.type,
            method = effect.method,
            tickInterval = effect.tickInterval,
            value = effect.value
        )
}