package com.roukanken.veilthroch.datacenter.mapping

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class DcName(val value: String)