package com.roukanken.veilthroch.datacenter.mapping

class MappingException : RuntimeException {

    constructor(string: String) : super(string)
    constructor(string: String, cause: Throwable) : super(string, cause)

}
