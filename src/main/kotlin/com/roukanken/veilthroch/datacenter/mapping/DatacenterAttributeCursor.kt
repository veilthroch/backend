package com.roukanken.veilthroch.datacenter.mapping

import com.roukanken.veilthroch.datacenter.structure.Datacenter
import com.roukanken.veilthroch.datacenter.structure.DatacenterAddress
import com.roukanken.veilthroch.datacenter.structure.DatacenterAttribute
import java.lang.reflect.InvocationTargetException
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KProperty
import kotlin.reflect.full.createType
import kotlin.reflect.full.isSubtypeOf
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.javaField

class DatacenterAttributeCursor(private val datacenter: Datacenter,
                                private val attribute: DatacenterAttribute) {
    val name: String
        get() = datacenter.names[attribute.nameIndex]

    val value: Any
        get() = when (attribute.value) {
            is DatacenterAddress -> datacenter.values[attribute.value]
            else -> attribute.value
        }

    fun fillAttribute(pojo: Any) {
        when (val property = findProperty(pojo::class, name)) {
            is KMutableProperty<*> -> setAttribute(pojo, property)
            null -> Unit
            else -> throw MappingException("Property $name on ${pojo::class.simpleName} is not settable")
        }
    }

    private fun setAttribute(pojo: Any, property: KMutableProperty<*>) {
        try {
            property.setter.call(pojo, value)
        } catch (e: InvocationTargetException) {
            throw MappingException("Setting '$name' on ${pojo::class.simpleName} class threw error", e)
        } catch (e: IllegalArgumentException) {
            throw MappingException("Setting '$name' on ${pojo::class.simpleName} class threw error", e)
        }
    }
}

internal fun Sequence<DatacenterAttributeCursor>.fillAttributes(pojo: Any) {
    this.forEach { it.fillAttribute(pojo) }

    val unfilledPropertyNames = pojo::class.memberProperties
        .filter { shouldCheckProperty(it) }
        .filter { runCatching { it.getter.call(pojo) }.isFailure }
        .map { it.name }

    if (unfilledPropertyNames.isNotEmpty()) {
        val names = unfilledPropertyNames.joinToString(", ")
        throw MappingException(
            "Object of class ${pojo::class.simpleName} has unset non-nullable attributes: [$names]"
        )
    }
}

private fun shouldCheckProperty(property: KProperty<*>) =
    when {
        property.returnType.isSubtypeOf(String::class.createType()) -> true
        property.returnType.isSubtypeOf(Int::class.createType()) -> true
        property.returnType.isSubtypeOf(Float::class.createType()) -> true
        property.returnType.isSubtypeOf(Boolean::class.createType()) -> true
        else -> false
    }

internal fun <T : Any> findProperty(clazz: KClass<T>, name: String): KProperty<*>? {
    val properties = clazz.memberProperties.filter { prop ->
        val dcName = prop.javaField?.getAnnotation(DcName::class.java)

        when {
            prop.name.toLowerCase() == name.toLowerCase() -> true
            prop.name.toLowerCase() == name.toLowerCase() + "s" -> true
            dcName != null && dcName.value == name -> true
            else -> false
        }
    }

    return when (properties.size) {
        0 -> null
        1 -> properties.first()
        else -> throw MappingException("Found multiple matching properties for '$name': [${properties.joinToString { it.name }}]")
    }
}

internal fun Datacenter.getAttributeCursor(address: DatacenterAddress): DatacenterAttributeCursor {
    return DatacenterAttributeCursor(this, this.attributes[address])
}

