package com.roukanken.veilthroch.datacenter.mapping

import com.roukanken.veilthroch.datacenter.structure.Datacenter
import com.roukanken.veilthroch.datacenter.structure.DatacenterAddress
import com.roukanken.veilthroch.datacenter.structure.DatacenterElement
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.isSubclassOf

class DatacenterElementCursor {

    val datacenter: Datacenter
    private val element: DatacenterElement

    val name
        get() = datacenter.names[element.nameIndex]

    constructor(datacenter: Datacenter) {
        this.datacenter = datacenter
        this.element = datacenter.elements[DatacenterAddress(0, 0)]
    }

    internal constructor(datacenter: Datacenter, element: DatacenterElement) {
        this.datacenter = datacenter
        this.element = element
    }

    fun children(): Sequence<DatacenterElementCursor> {
        return (0 until element.childrenCount).asSequence()
            .map { element.childrenAddress + it }
            .map { datacenter.getCursor(it) }
    }

    fun attributes(): Sequence<DatacenterAttributeCursor> {
        return (0 until element.attributeCount).asSequence()
            .map { element.attributeAddress + it }
            .map { datacenter.getAttributeCursor(it) }
    }

    fun <T : Any> mapTo(clazz: KClass<T>): T {
        val data = clazz.createInstance()

        attributes().fillAttributes(data)

        children()
            .groupBy { it.name }
            .forEach { (name, elements) ->
                fillElements(data, clazz, name, elements)
            }

        return data
    }

    private fun <T : Any> fillElements(data: T,
                                       clazz: KClass<T>,
                                       name: String,
                                       elements: List<DatacenterElementCursor>) {

        val property = findProperty(clazz, name) ?: return

        if (property !is KMutableProperty<*>)
            throw MappingException("Property ${clazz.simpleName}.${property.name} is not mutable")

        val isList = (property.returnType.classifier as KClass<*>).isSubclassOf(List::class)

        if (isList) {
            val elementClass = property.returnType.arguments.first().type?.classifier as KClass<*>
            val mapped = elements.map { it.mapTo(elementClass) }

            property.setter.call(data, mapped)
        } else {
            val nullable = property.returnType.isMarkedNullable

            when {
                elements.isEmpty() && nullable -> return
                elements.size == 1 -> {
                    val element = elements.first().mapTo(property.returnType.classifier as KClass<*>)
                    property.setter.call(data, element)
                    return
                }
                else -> throw MappingException("Found element singleton '$name' but datacenter has ${elements.size} elements")
            }
        }
    }
}

inline fun <reified T : Any> Datacenter.mapTo(): T {
    return this.getCursor().mapTo(T::class)
}

fun Datacenter.getCursor(): DatacenterElementCursor {
    return DatacenterElementCursor(this)
}

internal fun Datacenter.getCursor(address: DatacenterAddress): DatacenterElementCursor {
    return DatacenterElementCursor(this, this.elements[address])
}