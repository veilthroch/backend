package com.roukanken.veilthroch.datacenter.structure

internal class DatacenterSegmentedRegion<T>(stream: DatacenterInputStream, creator: (DatacenterInputStream) -> T) {

    private val count = stream.readUnsignedInt()

    private val elements = (0 until count).map { DatacenterRegion(stream, creator) }

    operator fun get(address: DatacenterAddress): T {
        return elements[address.segmentIndex][address.elementIndex]
    }

}
