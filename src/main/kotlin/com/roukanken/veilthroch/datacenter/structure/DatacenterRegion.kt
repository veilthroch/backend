package com.roukanken.veilthroch.datacenter.structure

internal class DatacenterRegion<T>(stream: DatacenterInputStream, creator: (DatacenterInputStream) -> T) {
    private val fullCount = stream.readUnsignedInt()

    private val usedCount = stream.readUnsignedInt()
    private val elements = (0 until fullCount).map { creator(stream) }

    internal operator fun get(index: Int): T {
        return elements[index]
    }

}
