package com.roukanken.veilthroch.datacenter.structure

/**
 * Class for representing datacenter structure as-is
 */
class Datacenter(stream: DatacenterInputStream) {

    internal val header = DatacenterHeader(stream)
    internal val extensions = DatacenterSimpleRegion(stream, false, ::DatacenterExtension)
    internal val attributes = DatacenterSegmentedRegion(stream, ::DatacenterAttribute)
    internal val elements = DatacenterSegmentedRegion(stream, ::DatacenterElement)
    internal val values = DatacenterStringTable(stream, 1024)
    internal val names = DatacenterStringTable(stream, 512)
    internal val footer = DatacenterFooter(stream)

}
