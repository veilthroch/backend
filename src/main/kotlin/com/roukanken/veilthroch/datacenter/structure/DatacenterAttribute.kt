package com.roukanken.veilthroch.datacenter.structure

class DatacenterAttribute(stream: DatacenterInputStream) {

    internal val nameIndex = stream.readUnsignedShort()
    private val hash = stream.readUnsignedShort()
    private val type = DatacenterType.fromHash(hash)

    internal val value: Any = when (type) {
        DatacenterType.INT -> stream.readInt()
        DatacenterType.FLOAT -> stream.readFloat()
        DatacenterType.STRING -> DatacenterAddress(stream)
        DatacenterType.BOOLEAN -> stream.readInt() == 1
        null -> throw IllegalArgumentException()
    }

}
