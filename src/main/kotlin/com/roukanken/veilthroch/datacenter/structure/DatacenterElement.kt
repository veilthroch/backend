package com.roukanken.veilthroch.datacenter.structure

internal class DatacenterElement(stream: DatacenterInputStream) {

    internal val nameIndex = stream.readUnsignedShort()
    internal val extension = stream.readUnsignedShort()
    internal val attributeCount = stream.readUnsignedShort()
    internal val childrenCount = stream.readUnsignedShort()
    internal val attributeAddress = DatacenterAddress(stream)
    internal val childrenAddress = DatacenterAddress(stream)

}
