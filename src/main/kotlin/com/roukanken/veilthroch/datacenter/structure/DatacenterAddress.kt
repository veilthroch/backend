package com.roukanken.veilthroch.datacenter.structure

internal class DatacenterAddress {

    internal val segmentIndex: Int
    internal val elementIndex: Int

    constructor(stream: DatacenterInputStream) {
        this.segmentIndex = stream.readUnsignedShort()
        this.elementIndex = stream.readUnsignedShort()
    }

    constructor(segment: Int, element: Int) {
        this.segmentIndex = segment
        this.elementIndex = element
    }

    operator fun plus(increase: Int) = DatacenterAddress(segmentIndex, elementIndex + increase)


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DatacenterAddress

        if (segmentIndex != other.segmentIndex) return false
        if (elementIndex != other.elementIndex) return false

        return true
    }

    override fun hashCode(): Int {
        var result = segmentIndex
        result = 31 * result + elementIndex
        return result
    }


}
