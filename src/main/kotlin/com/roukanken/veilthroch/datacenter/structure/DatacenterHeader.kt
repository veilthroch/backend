package com.roukanken.veilthroch.datacenter.structure

internal class DatacenterHeader(stream: DatacenterInputStream) {

    private val fileVersion = stream.readUnsignedInt()
    private val unknown1 = stream.readInt()
    private val unknown2 = stream.readShort()
    private val unknown3 = stream.readShort()
    private val clientVersion = stream.readUnsignedInt()
    private val unknown4 = stream.readInt()
    private val unknown5 = stream.readInt()
    private val unknown6 = stream.readInt()
    private val unknown7 = stream.readInt()

}
