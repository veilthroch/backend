package com.roukanken.veilthroch.datacenter.structure

internal class DatacenterSimpleRegion<T>(stream: DatacenterInputStream, val offByOne: Boolean, creator: (DatacenterInputStream) -> T) {
    private val count = stream.readUnsignedInt()

    private val elements = (0 until if (offByOne) count - 1 else count).map { creator(stream) }

    operator fun get(name: Int): T {
        return elements[if (offByOne) name - 1 else name]
    }
}
