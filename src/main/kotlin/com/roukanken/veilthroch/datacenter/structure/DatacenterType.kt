package com.roukanken.veilthroch.datacenter.structure

enum class DatacenterType(val code: Int) {
    INT(1), FLOAT(2), STRING(3), BOOLEAN(5);

    companion object {
        fun fromHash(hash: Int): DatacenterType? {
            var code = hash % 4
            if (code == 1) code = hash % 8
            if (code == 0) code = 3

            return values().find { it.code == code }
        }
    }
}
