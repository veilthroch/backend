package com.roukanken.veilthroch.datacenter.structure

internal class DatacenterSegmentedSimpleRegion<T>(stream: DatacenterInputStream, count: Int, creator: (DatacenterInputStream) -> T) {

    private val elements = (0 until count).map { DatacenterSimpleRegion(stream, false, creator) }

}
