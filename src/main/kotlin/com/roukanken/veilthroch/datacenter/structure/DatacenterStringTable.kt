package com.roukanken.veilthroch.datacenter.structure

internal class DatacenterStringTable(stream: DatacenterInputStream, count: Int) {

    private val data = DatacenterSegmentedRegion(stream, DatacenterInputStream::readChar)
    private val hashes = DatacenterSegmentedSimpleRegion(stream, count, ::DatacenterHash)
    private val addresses = DatacenterSimpleRegion(stream, true, ::DatacenterAddress)

    operator fun get(nameIndex: Int): String {
        if (nameIndex == 0) return "__placeholder__"
        return get(addresses[nameIndex])
    }

    operator fun get(address: DatacenterAddress): String {
        var result = ""
        var pointer = address

        while (data[pointer] != 0.toChar()) {
            result += data[pointer]
            pointer += 1
        }

        return result

    }

}
