package com.roukanken.veilthroch.datacenter.structure

internal class DatacenterHash(stream: DatacenterInputStream) {

    private val hash = stream.readUnsignedInt()
    private val length = stream.readUnsignedInt()
    private val index = stream.readUnsignedInt()
    private val address = DatacenterAddress(stream)

}
