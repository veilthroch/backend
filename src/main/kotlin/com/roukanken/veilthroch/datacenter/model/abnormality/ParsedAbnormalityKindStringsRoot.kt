package com.roukanken.veilthroch.datacenter.model.abnormality

import com.roukanken.veilthroch.datacenter.model.common.ParsedString

class ParsedAbnormalityKindStringsRoot {
    var strings = emptyList<ParsedString>()
}
