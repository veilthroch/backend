package com.roukanken.veilthroch.datacenter.model.abnormality

class ParsedAbnormalityRoot {
    var abnormals: List<ParsedAbnormality> = emptyList()
}