package com.roukanken.veilthroch.datacenter.model.abnormality

import kotlin.properties.Delegates

class ParsedAbnormalityEffect {
    var type by Delegates.notNull<Int>()
    var method by Delegates.notNull<Int>()
    lateinit var value: String
    var tickInterval by Delegates.notNull<Int>()

    var appearEffectId: Int? = null
    var appearShader: String? = null
    var arcaneFxId: Int? = null
    var attackEffectId: Int? = null
    var damageEffectId: Int? = null
    var disappearEffectId: Int? = null
    var disappearShader: String? = null
    var effectId: Int? = null
    var effectPart: String? = null
    var effectTime: Int? = null
    var isEnemyCheck: Int? = null
    var isStartPostProcessEffect: Boolean? = null
    var linkEffectDuration: Float? = null
    var linkEffectResource: String? = null
    var overlayEffectId: Int? = null
    var postProcessAppearTime: Int? = null
    var postProcessDisappearTime: Int? = null
    var postProcessEffectIds: String? = null
    var postProcessId: Int? = null
    var shaderOnAlly: String? = null
    var shaderOnMy: String? = null
    var shaderOnOther: String? = null
}
