package com.roukanken.veilthroch.datacenter.model.abnormality

import kotlin.properties.Delegates

class ParsedAbnormalityIcon {
    var abnormalityId by Delegates.notNull<Int>()
    var iconName: String? = null
}
