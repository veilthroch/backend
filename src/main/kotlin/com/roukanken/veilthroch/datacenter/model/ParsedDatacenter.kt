package com.roukanken.veilthroch.datacenter.model

import com.roukanken.veilthroch.datacenter.mapping.DcName
import com.roukanken.veilthroch.datacenter.model.abnormality.ParsedAbnormalityIconData
import com.roukanken.veilthroch.datacenter.model.abnormality.ParsedAbnormalityKindStringsRoot
import com.roukanken.veilthroch.datacenter.model.abnormality.ParsedAbnormalityRoot
import com.roukanken.veilthroch.datacenter.model.abnormality.ParsedAbnormalityStringsRoot

class ParsedDatacenter {
    @DcName("Abnormality")
    var abnormalities = emptyList<ParsedAbnormalityRoot>()
    var abnormalityIconData = emptyList<ParsedAbnormalityIconData>()

    @DcName("StrSheet_Abnormality")
    var abnormalityStrings = emptyList<ParsedAbnormalityStringsRoot>()

    @DcName("StrSheet_AbnormalityKind")
    lateinit var abnormalityKind: ParsedAbnormalityKindStringsRoot
}
