package com.roukanken.veilthroch.datacenter.model.common

import kotlin.properties.Delegates

class ParsedString {
    var id by Delegates.notNull<Int>()
    var name: String? = null
    var tooltip: String? = null
}
