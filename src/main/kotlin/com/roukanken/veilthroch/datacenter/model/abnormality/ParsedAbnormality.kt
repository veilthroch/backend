package com.roukanken.veilthroch.datacenter.model.abnormality

import kotlin.properties.Delegates

class ParsedAbnormality {
    var id by Delegates.notNull<Int>()

    var bySkillCategory: String? = null
    var canHold: Boolean? = null
    var group: String? = null
    var infinity by Delegates.notNull<Boolean>()
    var isBlockOtherSpecialShader: Boolean? = null
    var isBuff by Delegates.notNull<Boolean>()
    var isHideOnRaid: Boolean? = null
    var isHideOnRefresh by Delegates.notNull<Boolean>()
    lateinit var isShow: String
    var kind by Delegates.notNull<Int>()
    var level by Delegates.notNull<Int>()
    var notCareDeath by Delegates.notNull<Boolean>()
    var priority by Delegates.notNull<Int>()
    var property by Delegates.notNull<Int>()
    lateinit var time: String
    var useDamageFont: Boolean? = null

    var abnormalityEffects: List<ParsedAbnormalityEffect> = emptyList()
}
