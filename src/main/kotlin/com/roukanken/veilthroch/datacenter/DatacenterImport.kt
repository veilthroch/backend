package com.roukanken.veilthroch.datacenter

import com.roukanken.veilthroch.datacenter.collect.AbnormalityCollector
import com.roukanken.veilthroch.datacenter.mapping.mapTo
import com.roukanken.veilthroch.datacenter.model.ParsedDatacenter
import com.roukanken.veilthroch.datacenter.structure.Datacenter
import com.roukanken.veilthroch.logger
import com.roukanken.veilthroch.model.tera.common.TeraPatch
import com.roukanken.veilthroch.repository.AbnormalityRepository
import kotlinx.coroutines.coroutineScope
import org.springframework.stereotype.Service

@Service
class DatacenterImport(val abnormalityRepository: AbnormalityRepository) {

    var log = logger()

    suspend fun importDatacenter(patch: TeraPatch, datacenter: Datacenter) = coroutineScope {
        log.info("Mapping datacenter")
        val parsed = datacenter.mapTo<ParsedDatacenter>()

        log.info("Collecting abnormals")
        val abnormalities = AbnormalityCollector.collect(patch, parsed)

        // TODO: batch calls to db?
        log.info("Mapping abnormals")
        val updated = abnormalities.map {
            abnormalityRepository.upsert(it)
        }.sum()
        log.info("Done! Updated $updated documents")
    }

}