package com.roukanken.veilthroch.endpoint

import com.roukanken.veilthroch.datacenter.DatacenterImport
import com.roukanken.veilthroch.datacenter.structure.Datacenter
import com.roukanken.veilthroch.datacenter.structure.DatacenterInputStream
import com.roukanken.veilthroch.model.tera.common.Region
import com.roukanken.veilthroch.model.tera.common.TeraPatch
import graphql.kickstart.tools.GraphQLMutationResolver
import org.springframework.stereotype.Component
import java.time.Duration
import java.time.LocalDateTime

@Suppress("unused")
@Component
class Mutation(private val datacenterImport: DatacenterImport) : GraphQLMutationResolver {

    suspend fun importTestDc(): String {
        val start = LocalDateTime.now()

        val stream = this::class.java.getResourceAsStream("/B84_EU.dat")
        datacenterImport.importDatacenter(TeraPatch(Region.EU, "42"), Datacenter(DatacenterInputStream(stream)))

        return (Duration.between(start, LocalDateTime.now())).toString()
    }

}
