package com.roukanken.veilthroch.endpoint

import com.roukanken.veilthroch.model.tera.common.TeraId
import com.roukanken.veilthroch.repository.AbnormalityRepository
import com.roukanken.veilthroch.repository.PassivityRepository
import graphql.kickstart.tools.GraphQLQueryResolver
import org.springframework.stereotype.Component

@Suppress("unused")
@Component
class Query(
    private val passivityRepository: PassivityRepository,
    private val abnormalityRepository: AbnormalityRepository
) : GraphQLQueryResolver {

    suspend fun getPassivity(id: TeraId) = passivityRepository.findOne(id)

    suspend fun getAbnormality(id: TeraId) = abnormalityRepository.findOne(id)
}
