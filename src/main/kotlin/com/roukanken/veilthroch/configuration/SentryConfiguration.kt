package com.roukanken.veilthroch.configuration

import graphql.ExceptionWhileDataFetching
import graphql.GraphQLError
import graphql.kickstart.execution.error.GraphQLErrorHandler
import io.sentry.Sentry
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component

@Configuration
class SentryConfiguration

@Component
class ReportingGraphQLErrorHandler : GraphQLErrorHandler {
    override fun processErrors(p0: MutableList<GraphQLError>?): MutableList<GraphQLError>? {
        if (p0 == null)
            return null

        p0.stream()
            .filter { isServerError(it) }
            .forEach { reportError(it) }

        return p0
    }

    private fun isServerError(error: GraphQLError): Boolean {
        return error is ExceptionWhileDataFetching || error is Throwable
    }

    private fun reportError(error: GraphQLError) {
        if (error is Throwable)
            Sentry.capture(error)

        if (error is ExceptionWhileDataFetching) {
            Sentry.getContext().addTag("path", "/" + error.path.joinToString())
            Sentry.capture(error.exception)
        }
    }

}