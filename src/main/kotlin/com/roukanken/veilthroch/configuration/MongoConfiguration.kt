package com.roukanken.veilthroch.configuration

import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.MongoCredential
import org.litote.kmongo.coroutine.CoroutineClient
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class MongoConfiguration {

    // TODO: configure settings
    @Bean
    fun mongoClient(configurationProperties: MongoConfigurationProperties) =
        KMongo.createClient(configurationProperties.toMongo()).coroutine

    @Bean
    fun database(mongoClient: CoroutineClient, configurationProperties: MongoConfigurationProperties) =
        mongoClient.getDatabase(configurationProperties.database ?: "test")
}

@ConfigurationProperties("mongo")
@ConstructorBinding
class MongoConfigurationProperties(
    val url: String = "mongodb://localhost",
    val auth: Boolean = false,
    val username: String?,
    val password: String?,
    val database: String?
) {
    fun toMongo(): MongoClientSettings {
        val builder = MongoClientSettings.builder()
            .applyConnectionString(ConnectionString(url))

        if (auth && password != null && username != null) {
            val credential = MongoCredential.createCredential(username, "admin", password.toCharArray())
            builder.credential(credential)
        }

        return builder.build()
    }

}