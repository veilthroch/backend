package com.roukanken.veilthroch.configuration

import graphql.GraphQLError
import graphql.kickstart.execution.GraphQLObjectMapper
import graphql.kickstart.execution.GraphQLQueryInvoker
import graphql.kickstart.execution.error.DefaultGraphQLErrorHandler
import graphql.kickstart.execution.error.GraphQLErrorHandler
import graphql.kickstart.servlet.GraphQLConfiguration
import graphql.kickstart.servlet.core.GraphQLServletListener
import graphql.kickstart.servlet.input.BatchInputPreProcessor
import graphql.kickstart.servlet.input.GraphQLInvocationInputFactory
import graphql.kickstart.spring.web.boot.GraphQLServletProperties
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary


@Configuration
class GraphQLSpringConfiguration {

    @Bean
    @Primary
    fun errorHandler(reporting: ReportingGraphQLErrorHandler): GraphQLErrorHandler {
        return ComposingGraphQLErrorHandler(listOf(reporting, DefaultGraphQLErrorHandler()))
    }

    @Bean
    fun graphQLServletConfiguration(invocationInputFactory: GraphQLInvocationInputFactory?,
                                    queryInvoker: GraphQLQueryInvoker?,
                                    graphQLObjectMapper: GraphQLObjectMapper?,
                                    listeners: List<GraphQLServletListener>? = emptyList(),
                                    graphQLServletProperties: GraphQLServletProperties,
                                    batchInputPreProcessor: BatchInputPreProcessor?,
                                    @Value("\${graphql.servlet.async-timeout:60}") asyncTimeout: Long
    ): GraphQLConfiguration {
        return GraphQLConfiguration.with(invocationInputFactory)
            .with(queryInvoker)
            .with(graphQLObjectMapper)
            .with(listeners)
            .with(graphQLServletProperties.isAsyncModeEnabled)
            .with(graphQLServletProperties.subscriptionTimeout)
            .with(batchInputPreProcessor)
            .with(graphQLServletProperties.contextSetting)
            .asyncTimeout(asyncTimeout)
            .build()
    }
}

class ComposingGraphQLErrorHandler(private val errorHandlers: List<GraphQLErrorHandler>) : GraphQLErrorHandler {
    override fun processErrors(p0: MutableList<GraphQLError>?): MutableList<GraphQLError>? {
        var list = p0

        errorHandlers.forEach {
            list = it.processErrors(list)
        }

        return list
    }
}