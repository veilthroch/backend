package com.roukanken.veilthroch.model.tera.abnormality

data class AbnormalityKind(
    val id: Int,
    val description: String? = null
)
