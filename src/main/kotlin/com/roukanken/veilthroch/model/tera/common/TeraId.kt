package com.roukanken.veilthroch.model.tera.common

data class TeraId(
    val id: Int,
    val region: Region,
    val patch: String
) {
    fun asPatch(): TeraPatch {
        return TeraPatch(region, patch)
    }
}