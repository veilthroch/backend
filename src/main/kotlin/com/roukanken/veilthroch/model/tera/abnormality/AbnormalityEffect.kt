package com.roukanken.veilthroch.model.tera.abnormality

class AbnormalityEffect(
    val type: Int,
    val method: Int,
    value: String,
    val tickInterval: Int
) {
    private val _value = value
    val value: Any
        get() = _value.toIntOrNull() ?: _value.toDoubleOrNull() ?: _value
}
