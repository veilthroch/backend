package com.roukanken.veilthroch.model.tera.common

data class TeraPatch(
    val region: Region,
    val patch: String
) {
    // TODO: find a good name for this
    fun asObject(id: Int): TeraId {
        return TeraId(id, region, patch)
    }
}