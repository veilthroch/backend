package com.roukanken.veilthroch.model.tera.abnormality

import com.roukanken.veilthroch.model.tera.common.TeraId
import com.roukanken.veilthroch.model.tera.common.TeraObject
import java.math.BigInteger

data class Abnormality(
    override val id: TeraId,

    val name: String? = null,
    val tooltip: String? = null,
    val icon: String? = null,

    val kind: AbnormalityKind,
    val priority: Int,
    val level: Int,
    val time: BigInteger,

    val isInfinite: Boolean,
    val isBuff: Boolean,
    val isHidden: Boolean,

    val effects: List<AbnormalityEffect> = emptyList()
) : TeraObject()