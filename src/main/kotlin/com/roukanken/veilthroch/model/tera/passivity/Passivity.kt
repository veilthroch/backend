package com.roukanken.veilthroch.model.tera.passivity

import com.roukanken.veilthroch.model.tera.common.TeraId
import com.roukanken.veilthroch.model.tera.common.TeraObject

data class Passivity(
    override val id: TeraId
) : TeraObject()