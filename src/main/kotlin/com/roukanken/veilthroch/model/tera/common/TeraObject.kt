package com.roukanken.veilthroch.model.tera.common

abstract class TeraObject {
    abstract val id: TeraId

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TeraObject

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}