
import com.roukanken.veilthroch.model.tera.passivity.Passivity

data class Card(val id: Int,
                val category: Int,
                val level: Int,
                val effect: String?,
                val itemsNeeded: Int,
                val cost: Int,
                val superiorCard: Card?,
                val exp: Int,
                val source: String,
                val lore: String,
                val passivities: List<Passivity>)