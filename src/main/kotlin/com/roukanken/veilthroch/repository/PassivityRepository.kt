package com.roukanken.veilthroch.repository

import com.roukanken.veilthroch.model.tera.passivity.Passivity

interface PassivityRepository : TeraObjectRepository<Passivity>