package com.roukanken.veilthroch.repository

import com.roukanken.veilthroch.model.tera.abnormality.Abnormality

interface AbnormalityRepository : TeraObjectRepository<Abnormality>