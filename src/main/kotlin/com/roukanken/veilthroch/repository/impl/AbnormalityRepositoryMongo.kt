package com.roukanken.veilthroch.repository.impl

import com.roukanken.veilthroch.model.tera.abnormality.Abnormality
import com.roukanken.veilthroch.repository.AbnormalityRepository
import org.litote.kmongo.coroutine.CoroutineDatabase
import org.springframework.stereotype.Repository

@Repository
class AbnormalityRepositoryMongo(database: CoroutineDatabase) : TeraObjectRepositoryMongo<Abnormality>(database, Abnormality::class), AbnormalityRepository