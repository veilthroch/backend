package com.roukanken.veilthroch.repository.impl

import com.mongodb.client.model.UpdateOptions
import com.roukanken.veilthroch.model.tera.common.TeraId
import com.roukanken.veilthroch.model.tera.common.TeraObject
import com.roukanken.veilthroch.repository.TeraObjectRepository
import kotlinx.coroutines.runBlocking
import org.litote.kmongo.coroutine.CoroutineCollection
import org.litote.kmongo.coroutine.CoroutineDatabase
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.eq
import org.litote.kmongo.util.KMongoUtil
import kotlin.reflect.KClass

open class TeraObjectRepositoryMongo<T : TeraObject>(database: CoroutineDatabase, clazz: KClass<T>) : TeraObjectRepository<T> {
    protected val collection: CoroutineCollection<T> = database.database.getCollection(KMongoUtil.defaultCollectionName(clazz), clazz.java).coroutine

    init {
        runBlocking {
            collection.ensureUniqueIndex(TeraObject::id)
        }
    }

    override suspend fun findOne(id: TeraId) =
        collection.findOne(TeraObject::id eq id)

    override suspend fun upsert(data: T): Long {
        val options = UpdateOptions()
        options.upsert(true)

        return collection.updateOne(
            TeraObject::id eq data.id, data, options
        ).modifiedCount
    }

}