package com.roukanken.veilthroch.repository.impl

import com.roukanken.veilthroch.model.tera.passivity.Passivity
import com.roukanken.veilthroch.repository.PassivityRepository
import org.litote.kmongo.coroutine.CoroutineDatabase
import org.springframework.stereotype.Repository

@Repository
class PassivityRepositoryMongo(database: CoroutineDatabase) : TeraObjectRepositoryMongo<Passivity>(database, Passivity::class), PassivityRepository