package com.roukanken.veilthroch.repository

import com.roukanken.veilthroch.model.tera.common.TeraId

interface TeraObjectRepository<T> {
    suspend fun findOne(id: TeraId): T?
    suspend fun upsert(data: T): Long
}