import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.3.0.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    kotlin("jvm") version "1.4-M2"
    kotlin("plugin.spring") version "1.3.72"
    kotlin("plugin.jpa") version "1.3.72"
    kotlin("kapt") version "1.4-M2"
    jacoco
}

repositories {
    jcenter()
    mavenCentral()
    maven("https://oss.jfrog.org/artifactory/oss-snapshot-local")
    maven("https://dl.bintray.com/kotlin/kotlin-eap")
    maven("https://kotlin.bintray.com/kotlinx")
}

group = "com.roukanken"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

configurations {
    compileOnly {
        extendsFrom(configurations.kapt.get())
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.springframework.boot:spring-boot-starter")

    implementation("com.graphql-java-kickstart:graphql-spring-boot-starter:latest.integration")
    implementation("com.graphql-java:graphql-java-tools:latest.release")
    implementation("io.sentry:sentry-spring-boot-starter:latest.release")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.7")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:1.3.7")
    implementation("org.litote.kmongo:kmongo-coroutine:latest.release")

    runtimeOnly("com.graphql-java-kickstart:playground-spring-boot-starter:latest.release")

    developmentOnly("org.springframework.boot:spring-boot-devtools")

    kapt("org.springframework.boot:spring-boot-configuration-processor")

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
        exclude(group = "org.assertj", module = "assertj-core")
        exclude(module = "mockito-core")
    }
    testImplementation("com.graphql-java-kickstart:graphql-spring-boot-starter-test:latest.release")
    testImplementation("io.mockk:mockk:latest.release")
    testImplementation("com.ninja-squad:springmockk:latest.release")

    testImplementation("io.strikt:strikt-core:latest.release")
    testImplementation("io.strikt:strikt-mockk:latest.release")

}

tasks.test {
    useJUnitPlatform {
        excludeTags("integrationTest")
    }
}

tasks.register<Test>("integrationTest") {
    useJUnitPlatform {
        includeTags("integrationTest")
    }

    maxHeapSize = "3G"
}

tasks.jacocoTestReport {
    executionData.setFrom(
        fileTree(project.projectDir) {
            include("**/*.exec")
        }
    )

    reports {
        xml.isEnabled = true
        xml.destination = File("${buildDir}/reports/jacoco/report.xml")
    }
}

tasks.withType<Test> {
    finalizedBy(tasks.jacocoTestReport)
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}
